import 'package:flutter/material.dart';

class CustomTextBG extends StatelessWidget {
    final double width;
    final String text;

    CustomTextBG({
        required this.width,
        required this.text
    });

    @override
    Widget build(BuildContext context) {
        var purpleBar = Container(
            width: 3,
            height: 21,
            color: Color.fromRGBO(132, 101, 255, 1),
        );

        var clippedRectangle = ClipPath(
            child: Container(
                width: width,
                height: 21,
                color: Colors.black,
            ),
            clipper: ContactBoxClipper(),
        );

        var textBody = Stack(
            children: [
                clippedRectangle,
                Positioned(
                    left: 7,
                    top: 2,
                    child: Text(
                        text, 
                        style: TextStyle(
                            fontFamily: 'Allerta-Stencil', 
                            fontSize: 12, 
                            color: Colors.white
                        )
                    )
                )
            ]
        );

        return Container(
            margin: EdgeInsets.only(right: 9.5),
            child: Row(
                children: [
                    purpleBar,
                    textBody
                ]
            )
        );
    }
}

class ContactBoxClipper extends CustomClipper<Path> {
    @override
    Path getClip(Size size) {
        var path = new Path();
        path.lineTo(0, 0);
        path.lineTo(0, size.height);
        path.lineTo(size.width, size.height);
        path.lineTo(size.width-10, 0);
        path.close();
        return path;
    }

    @override
    bool shouldReclip(CustomClipper<Path> oldClipper) => false;
}