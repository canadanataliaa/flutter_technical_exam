import 'package:flutter/material.dart';

class MyAccountCard extends StatelessWidget {
    final String title;
	final String image;
	final String name;
	final String email;
    final bool fullUnderline;
	final String assembler;

	const MyAccountCard({
        required this.title,
		required this.image,
		required this.name,
		required this.email,
        required this.fullUnderline,
		required this.assembler
	});

	@override
	Widget build(BuildContext context) {
		var h1Style = TextStyle(fontFamily: 'Allerta-Stencil', fontSize: 22, letterSpacing: -1.3);
		var h2Style = TextStyle(fontFamily: 'Allerta-Stencil', fontSize: 16, letterSpacing: -1.3);
		var subStyle = TextStyle(fontFamily: 'Mulish', fontSize: 12);
		var subUnderlineStyle = TextStyle(fontFamily: 'Mulish', fontSize: 12, decoration: TextDecoration.underline);

		Widget avatar = Container(
			child: Image.asset(image),
		);

		var txtEmailDesign = TextSpan(
            text: email.substring(0,8),
            style: subStyle,
            children: [
                TextSpan(
                    text: email.substring(8),
                    style: subUnderlineStyle
                )
            ]
        );

		Widget details = Container(
            margin: EdgeInsets.only(left: 16),
			child: Column(
				crossAxisAlignment: CrossAxisAlignment.start,
				children: [
					Text(name, style: h2Style),
                    SizedBox(height: 2),
                    if(!fullUnderline)
                    Text.rich(txtEmailDesign),
                    if(fullUnderline)
                    Text(email, style: subUnderlineStyle),
					SizedBox(height: 8),
					Text(assembler, style: subStyle)
				],
			),
		);

		return Card(
            margin: EdgeInsets.only(left: 16, top: 32, bottom: 36),
            elevation: 0,
			child: Column(
				crossAxisAlignment: CrossAxisAlignment.start,
				children: [
					Text(title, style: h1Style),
					SizedBox(height: 16),
					Row(
						children: [
							avatar,
							details
						],
					)
				]
			)
		);
	}
}