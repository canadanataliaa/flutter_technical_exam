import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:time_tracking_app/widgets/custom_button.dart';

class CommentCard extends StatelessWidget {
    @override
    Widget build(BuildContext context) {
        Widget abortBtn = InkWell(
            onTap: () {},
            child: Container(
                child: Text('Abbrechen'),
            )
        );

        return Card(
            elevation: 0,
            child: Column(
                children: [
                    TextField(
                        keyboardType: TextInputType.multiline,
                        textCapitalization: TextCapitalization.sentences,
                        maxLines: 5,
                        cursorHeight: 20,
                        decoration: InputDecoration(
                            contentPadding: EdgeInsets.all(20),
                            fillColor: Color.fromRGBO(245, 245, 245, 1),
                            filled: true,
                            border: InputBorder.none,
                            prefixIcon: Padding(
                                padding: EdgeInsets.only(left: 10, bottom: 40, right: 10),
                                child: Image.asset('assets/images/profile_avatar.png')
                            ),
                            hintText: 'Bemerkung hinzufügen',
                            hintStyle: TextStyle(
                                fontFamily: 'Mulish',
                                fontSize: 16,
                                fontStyle: FontStyle.italic,
                                color: Colors.black45
                            )
                        )
                    ),
                    Container(
                        margin: EdgeInsets.only(top: 32),
                        child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                                abortBtn,
                                SizedBox(width: 25),
                                CustomButton(
                                    title: 'Speichern',
                                    routeTo: '/add-time-tracking',
                                    width: 115,
                                    height: 42
                                )
                            ]
                        )
                    )
                ]
            )
        );
    }
}