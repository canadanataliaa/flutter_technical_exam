import 'package:flutter/material.dart';
import 'package:flutter_time_picker_spinner/flutter_time_picker_spinner.dart';

class TimePickerCard extends StatefulWidget {
    final String title;
    final Color borderColor;

    TimePickerCard({
        required this.title,
        required this.borderColor 
    });

    @override
    _TimePickerCardState createState() => _TimePickerCardState();
}

class _TimePickerCardState extends State<TimePickerCard> {
    DateTime _time = DateTime.now();

    Widget createTimePicker(String subtitle, Color borderColor) {
        return Container(
            width: 165,
            decoration: BoxDecoration(
                border: Border(bottom: BorderSide(color: borderColor, width: 4.1))
            ),
            child: Stack(
                children: [
                    TimePickerSpinner(
                        is24HourMode: true,
                        minutesInterval: 15,
                        normalTextStyle: TextStyle(
                            fontFamily: 'Allerta-Stencil',
                            fontSize: 14,
                            color: Colors.grey.shade300
                        ),
                        highlightedTextStyle: TextStyle(
                            fontFamily: 'Allerta-Stencil',
                            fontSize: 16,
                            color: Colors.black
                        ),
                        itemHeight: 40,
                        itemWidth: 30,
                        spacing: 1,
                        isForce2Digits: true,
                        onTimeChange: (time) {
                            setState(() {
                                _time = time;
                            });
                        },
                    ),
                    Positioned(
                        top: 51.9,
                        left: 28,
                        child: Image.asset('assets/icons/picker.png')
                    ),
                    Positioned(
                        top: 50.8,
                        left: 85,
                        child: Text(':', style: TextStyle(fontSize: 15))
                    ),
                    Positioned(
                        top: 70,
                        left: 60,
                        child: Text(
                            subtitle, 
                            style: TextStyle(
                                fontFamily: 'Allerta-Stencil',
                                fontSize: 12,
                                color: Colors.grey.shade500
                            )
                        )
                    ),
                    Positioned(
                        top: 51.3,
                        left: 116,
                        child: Text(
                            'Uhr.', 
                            style: TextStyle(
                                fontFamily: 'Allerta-Stencil',
                                fontSize: 15,
                                color: Colors.black
                            )
                        )
                    )
                ]
            )
        );
    }

    @override
    Widget build(BuildContext context) {
        return Card(
            margin: EdgeInsets.only(bottom: 32),
            elevation: 0,
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                    Text(
                        widget.title,
                        style: TextStyle(
                            fontFamily: 'Allerta-Stencil', 
                            fontSize: 22,
                            letterSpacing: -1.3,
                        )
                    ),
                    SizedBox(height: 16),
                    Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                            createTimePicker('Beginn', widget.borderColor),
                            createTimePicker('Ende', widget.borderColor),
                        ]
                    )
                ]
            )
        );
    }
}

