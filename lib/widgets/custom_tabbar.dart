import 'package:flutter/material.dart';

class CustomTabBar extends StatelessWidget {
    final List<String> tabTitles;
    final TabController controller;

    CustomTabBar({
        required this.tabTitles,
        required this.controller,
    });

    @override
    Widget build(BuildContext context) {
        return Container(
            margin: EdgeInsets.fromLTRB(18, 16, 18, 0),
            child: TabBar(
                controller: controller,
                indicator: BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage('assets/images/line_background.png'),
                        repeat: ImageRepeat.repeatX,
                    ),
                    border: Border(bottom: BorderSide(width: 3, color: Color.fromRGBO(132, 101, 255, 1)))
                ),
                labelPadding: EdgeInsets.all(1),
                labelStyle: TextStyle(fontFamily: 'Allerta-Stencil', fontSize: 18, letterSpacing: -1.3),
                labelColor: Colors.black,
                tabs: [
                    Tab(text: tabTitles[0]),
                    Tab(text: tabTitles[1]),
                ]
            )
        );
    }
}