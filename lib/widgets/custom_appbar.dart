import 'package:flutter/material.dart';

class CustomAppBar extends StatelessWidget implements PreferredSizeWidget {
    final String leading;
    final Color? leadingBG;
    final String? title;
    final String? subtitle;
    final List? trailingWH;
    final Color bgColor;
    final Color fontColor;
    final String routeTo;

    CustomAppBar({
        required this.leading,
        this.leadingBG,
        this.title,
        this.subtitle,
        this.trailingWH,
        required this.bgColor,
        required this.fontColor,
        required this.routeTo
    });

    @override
    Size get preferredSize => const Size.fromHeight(60);

    @override
    Widget build(BuildContext context) {
        Widget customMenuBtn = IconButton(
            icon: Container(
                width: 42,
                height: 42,
                decoration: BoxDecoration(
                    shape: BoxShape.circle, 
                    color: (leadingBG != null) ? leadingBG : null
                ),
                child: Image.asset(leading)
            ), 
            onPressed: () {
                if (routeTo.isNotEmpty) {
                    Navigator.pushNamed(context, routeTo);
                } else {
                    Navigator.pop(context);
                }
            }
        );

        return AppBar(
            backgroundColor: bgColor,
            elevation: 0,
            leadingWidth: 75,
            leading: customMenuBtn,
            titleSpacing: 1,
            actions: [ 
                if(trailingWH != null) 
                Container(
                    width: trailingWH![1],
                    height: trailingWH![2],
                    margin: EdgeInsets.only(right: 40),
                    child: Image.asset('assets/images/flutter_logo.png'),
                ) 
            ],
            title: (title != null && subtitle != null) 
                ? Container(
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                            Text(title as String, style: TextStyle(fontFamily: 'Allerta-Stencil', fontSize: 22, color: fontColor)),
                            Text(subtitle as String, style: TextStyle(fontFamily: 'Allerta-Stencil', fontSize: 12, color: fontColor)),
                        ]
                    ),
                ) 
                : (title != null && subtitle == null) 
                ? Text(title as String, style: TextStyle(fontFamily: 'Allerta-Stencil', fontSize: 22, color: fontColor)) 
                : null
        );
    }
}