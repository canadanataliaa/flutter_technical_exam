import 'package:flutter/material.dart';

class AddressCard extends StatelessWidget {
    final String title;
	final String image;
	final String name;
    final String street;
    final String city;

	const AddressCard({
        required this.title,
		required this.image,
		required this.name,
		required this.street,
        required this.city
	});

	@override
	Widget build(BuildContext context) {
		var h1Style = TextStyle(fontFamily: 'Allerta-Stencil', fontSize: 22, letterSpacing: -1.3);
		var h2Style = TextStyle(fontFamily: 'Allerta-Stencil', fontSize: 16, letterSpacing: -1.3);
		var subStyle = TextStyle(fontFamily: 'Mulish', fontSize: 12, color: Color.fromRGBO(25, 29, 38, 0.7));

		Widget avatar = Container(
			child: Image.asset(image),
		);

		Widget details = Container(
            margin: EdgeInsets.only(left: 16),
			child: Column(
				crossAxisAlignment: CrossAxisAlignment.start,
				children: [
					Text(name, style: h2Style),
                    SizedBox(height: 2),
					Text(street, style: subStyle),
					Text(city, style: subStyle)
				],
			),
		);

		return Card(
            margin: EdgeInsets.only(left: 16, bottom: 37),
            elevation: 0,
			child: Column(
				crossAxisAlignment: CrossAxisAlignment.start,
				children: [
					Text(title, style: h1Style),
					SizedBox(height: 16),
					Row(
						children: [
							avatar,
							details
						],
					)
				]
			)
		);
	}
}