import 'package:flutter/material.dart';

class ContactCard extends StatelessWidget {
    final String title;
	final String image;
	final String telephone;
    final String fax;
    final String mobile;
    final String email;
    final String website;

	const ContactCard({
        required this.title,
		required this.image,
		required this.telephone,
		required this.fax,
        required this.mobile,
        required this.email,
        required this.website
	});

	@override
	Widget build(BuildContext context) {
		var h1Style = TextStyle(fontFamily: 'Allerta-Stencil', fontSize: 22, letterSpacing: -1.3);
		var h2Style = TextStyle(fontFamily: 'Allerta-Stencil', fontSize: 16, letterSpacing: -1.3);
		var websiteStyle = TextStyle(fontFamily: 'Mulish', fontSize: 12, color: Color.fromRGBO(0, 164, 234, 1), decoration: TextDecoration.underline);

		Widget avatar = Container(
			child: Image.asset(image),
		);

		Widget details = Container(
            margin: EdgeInsets.only(left: 16),
			child: Column(
				crossAxisAlignment: CrossAxisAlignment.start,
				children: [
					Text('T: $telephone', style: h2Style),
                    SizedBox(height: 2),
					Text('F: $fax', style: h2Style),
					Text('M: $mobile', style: h2Style),
					Text('E: $email', style: h2Style),
                    SizedBox(height: 17),
                    Text(website, style: websiteStyle)
				],
			),
		);

		return Card(
            margin: EdgeInsets.only(left: 16, bottom: 37),
            elevation: 0,
			child: Column(
				crossAxisAlignment: CrossAxisAlignment.start,
				children: [
					Text(title, style: h1Style),
					SizedBox(height: 16),
					Row(
						children: [
							avatar,
							details
						],
					)
				]
			)
		);
	}
}