import 'package:flutter/material.dart';

class CustomTag extends StatelessWidget {
    final String tagTitle;
    final double width;
    final Color bgColor;

    CustomTag({
        required this.tagTitle,
        required this.width,
        required this.bgColor,
    });

    @override
    Widget build(BuildContext context) {
        return InkWell(
            child: Container(
                width: width,
                height: 32,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(50)),
                    color: bgColor
                ),
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                        Text(
                            tagTitle, 
                            style: TextStyle(
                                fontFamily: 'Mulish', 
                                fontSize: 14, 
                                color: Colors.white
                            )
                        ),
                        Image.asset('assets/icons/close_white.png')
                    ]
                )
            )
        );
    }
}