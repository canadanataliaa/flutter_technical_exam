import 'package:flutter/material.dart';

import '../screens/add_time_tracking/components/pause_screen.dart';
import '../screens/add_time_tracking/components/standby_time_screen.dart';
import '../screens/add_time_tracking/components/waiting_period_screen.dart';

import 'custom_tag.dart';

class AddorEditCard extends StatelessWidget {
    final String title;
    final Color btnColor;
    final String? tag;
    final double? tagWidth;
    final String? routeTo;
    final String category;
    final String projectId;

    AddorEditCard({
        required this.title,
        required this.btnColor,
        this.tag,
        this.tagWidth,
        this.routeTo,
        required this.category,
        required this.projectId
    });

    @override
    Widget build(BuildContext context) {
        Widget cardTitle = Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
                Text(
                    title,
                    style: TextStyle(
                        fontFamily: 'Allerta-Stencil', 
                        fontSize: 22,
                        letterSpacing: -1.3,
                    )
                ),
                SizedBox(height: 6),
                if (tag != null)
                CustomTag(
                    tagTitle: tag as String,
                    width: tagWidth as double,
                    bgColor: btnColor,
                ),
                if (tag == null)
                Text(
                    'hinzufügen oder bearbeiten', 
                    style: TextStyle(
                        fontFamily: 'Roboto', 
                        fontSize: 14, 
                        color: Color.fromRGBO(185, 185, 185, 1)
                    )
                )
            ]
        );

        Widget editBtn = InkWell(
            child: Image.asset('assets/icons/edit_grey.png'),
            onTap: () {
                // if (routeTo != null) {
                //     Navigator.pushnamed(context, routeTo as String);
                // }
            }
        );

        Widget addBtn = InkWell(
            child: Container(
                width: 42,
                height: 42,
                decoration: BoxDecoration(
                    shape: BoxShape.circle, 
                    color: btnColor
                ),
                child: Image.asset('assets/icons/add_white.png'),
            ),
            onTap: () async {
                if (routeTo != null) {
                    switch (routeTo) {
                        case '/pause':
                            await Navigator.push(context, 
                                MaterialPageRoute(
                                    builder: (context) => PauseScreen(category, projectId)
                                )
                            );
                            break;
                        case '/waiting-period':
                            await Navigator.push(context, 
                                MaterialPageRoute(
                                    builder: (context) => WaitingPeriodScreen()
                                )
                            );
                            break;
                        case '/standby-time':
                            await Navigator.push(context, 
                                MaterialPageRoute(
                                    builder: (context) => StandByTimeScreen(category, projectId)
                                )
                            );
                            break;
                    }
                }
            }
        );

        List<Widget> showActionButtons(String? name) {
            if (name != null) {
                return [
                    editBtn, 
                    SizedBox(width: 20),
                    addBtn
                ];
            } 
            return [addBtn];
        }
        
        return Card(
            margin: EdgeInsets.only(bottom: 32),
            elevation: 0,
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                    cardTitle,
                    Container(
                        child: Row(
                            children: [
                                ...showActionButtons(tag)
                            ]
                        )
                    )
                ]
            )
        );
    }
}