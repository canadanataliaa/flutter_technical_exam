import 'package:flutter/material.dart';

class CustomButton extends StatelessWidget {
    final String title;
    final double width;
    final double height;
    final String routeTo;

    CustomButton({ 
        required this.title,
        required this.width,
        required this.height,
        required this.routeTo
    });

    @override
    Widget build(BuildContext context) {
        return InkWell(
            child: Container(
                width: width,
                height: height,
                color: Colors.black,
                padding: EdgeInsets.fromLTRB(10, 12, 10, 12),
                child: Row(
                    children: [
                        Text(title, style: TextStyle(fontFamily: 'Roboto', fontSize: 14, color: Colors.white)),
                        Container(
                            margin: EdgeInsets.only(left: 13),
                            child: Image.asset('assets/icons/send_small.png'),
                        )
                    ]
                )
            ),
            onTap: () {
                if (routeTo.isNotEmpty) {
                    Navigator.pushNamed(context, routeTo);
                }
            }
        );
    }
}