// @dart=2.9
import 'package:flutter/material.dart';

import 'package:flutter/services.dart';
import 'package:time_machine/time_machine.dart';

import 'screens/add_time_tracking/add_time_tracking_screen.dart';
import 'screens/add_time_tracking/components/pause_screen.dart';
import 'screens/add_time_tracking/components/standby_time_screen.dart';
import 'screens/add_time_tracking/components/waiting_period_screen.dart';
import 'screens/calendar/calendar_screen.dart';
import 'screens/my_account/my_account_screen.dart';
import 'screens/navigation_drawer/navigation_drawer_screen.dart';
import 'screens/sign_in/sign_in_screen.dart';
import 'screens/time_tracking/time_tracking_screen.dart';
import 'screens/visiting/visiting_screen.dart';

void main() async {
    WidgetsFlutterBinding.ensureInitialized();
    await TimeMachine.initialize({'rootBundle': rootBundle});
    runApp(MyApp());
}

class MyApp extends StatelessWidget {
    @override
    Widget build(BuildContext context) {
        return MaterialApp(
            title: 'Time Tracking App',
            debugShowCheckedModeBanner: false,
            initialRoute: '/',
            routes: {
                '/': (context) => SignInScreen(),
                '/my-account': (context) => MyAccountScreen(),
                '/side-menu': (context) => NavigationDrawerScreen(),
                '/visiting': (context) => VisitingScreen(),
                '/time-tracking': (context) => TimeTrackingScreen(),
                '/calendar':  (context) => CalendarScreen(),
                '/add-time-tracking': (context) => AddTimeTrackingScreen(),
                '/pause': (context) => PauseScreen(),
                '/waiting-period': (context) => WaitingPeriodScreen(),
                '/standby-time': (context) => StandByTimeScreen(),
            },
        );
    }
}