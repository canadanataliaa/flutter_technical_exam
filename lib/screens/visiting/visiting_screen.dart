import 'package:flutter/material.dart';

import '../../widgets/address_card.dart';
import '../../widgets/contact_card.dart';
import '../../widgets/custom_appbar.dart';
import '../../widgets/custom_tabbar.dart';
import '../../widgets/myaccount_card.dart';

import 'components/tab_view.dart';

class VisitingScreen extends StatefulWidget {
    @override
    _VisitingScreenState createState() => _VisitingScreenState();
}

class _VisitingScreenState extends State<VisitingScreen> with SingleTickerProviderStateMixin {
    late TabController _controller;

    @override
    void initState() {
        super.initState();
        _controller = TabController(length: 2, vsync: this);
    }

    @override
    Widget build(BuildContext context) {
        return Scaffold(
            backgroundColor: Color.fromRGBO(245, 245, 245, 1),
            appBar: CustomAppBar(
                leading: 'assets/icons/menu.png',
                bgColor: Colors.white,
                fontColor: Colors.black,
                routeTo: '/side-menu'
            ),
            body: Column(
                children: [
                    CustomTabBar(
                        tabTitles: ['Meine Visitenkarte', 'Vorgesetzte'],
                        controller: _controller
                    ),
                    Expanded(
                        child: TabBarView(
                            controller: _controller,
                            children: [
                                // Meine Visitenkarte TabView
                                TabView(
                                    myAccountCard: MyAccountCard(
                                        title: 'Visitenkarte', 
                                        image: 'assets/images/profile_avatar.png', 
                                        name: 'Greg Neu', 
                                        email: 'greg.neu@f-bootcamp.com',
                                        fullUnderline: true, 
                                        assembler: 'Monteur'
                                    ),
                                    imageQR: 'assets/images/business_qr.png',
                                    addressCard: AddressCard(
                                        title: 'Adresse', 
                                        image: 'assets/icons/address.png', 
                                        name: 'Flutter Bootcamp', 
                                        street: '6783 Ayala Ave', 
                                        city: '1200 Metro Manila'
                                    ),
                                    contactCard: ContactCard(
                                        title: 'Kontakt', 
                                        image: 'assets/icons/user_kontakt.png', 
                                        telephone: '+49 1234 56 789 01', 
                                        fax: '+49 1234 56 789 01-2', 
                                        mobile: '+49 1234 56', 
                                        email: 'greg.neu@f-bootcamp.com', 
                                        website: 'www.flutter-bootcamp.com'
                                    )
                                ),
                                // Vorgesetzte TabView
                                TabView(
                                    myAccountCard: MyAccountCard(
                                        title: 'Vorgesetzte', 
                                        image: 'assets/images/profile_avatar.png', 
                                        name: 'Andero Mustermann', 
                                        email: 'andero.mustermann@f-bootcamp.com',
                                        fullUnderline: true, 
                                        assembler: 'Abteilungsleiter '
                                    ),
                                    imageQR: 'assets/images/superior_qr.png',
                                    addressCard: AddressCard(
                                        title: 'Adresse', 
                                        image: 'assets/icons/address.png', 
                                        name: 'Flutter Bootcamp', 
                                        street: '6783 Ayala Ave', 
                                        city: '1200 Metro Manila'
                                    ),
                                    contactCard: ContactCard(
                                        title: 'Kontakt', 
                                        image: 'assets/icons/user_kontakt.png', 
                                        telephone: '+49 1234 56 789 01', 
                                        fax: '+49 1234 56 789 01-2', 
                                        mobile: '+49 1234 56', 
                                        email: 'andreo.mustermann@f-bo...', 
                                        website: 'www.flutter-bootcamp.com'
                                    )
                                )
                            ]
                        )
                    )
                ]
            )
        );
    }
}