import 'package:flutter/material.dart';

class TabView extends StatelessWidget {
    final myAccountCard;
    final String imageQR;
    final addressCard;
    final contactCard;

    TabView({
        required this.myAccountCard,
        required this.imageQR,
        required this.addressCard,
        required this.contactCard
    });

    @override
    Widget build(BuildContext context) {
        Widget codeQR = Container(
            margin: EdgeInsets.only(bottom: 36),
            child: Image.asset(imageQR),
        );

        return Container(
            color: Colors.white,
            margin: EdgeInsets.only(top: 16, left: 16, right: 16),
            child: SingleChildScrollView(
                child: Container(
                    child: Column(
                        children: [
                            myAccountCard,
                            codeQR,
                            addressCard,
                            contactCard
                        ]
                    )
                )
            )
        );
    }
}