import 'package:flutter/material.dart';

import '../../widgets/custom_appbar.dart';

class CalendarScreen extends StatelessWidget {
    @override
    Widget build(BuildContext context) {
        return Scaffold(
            appBar: CustomAppBar(
                leading: 'assets/icons/close.png',
                leadingBG: Color.fromRGBO(224, 224, 224, 1),
                title: '2021',
                bgColor: Colors.white,
                fontColor: Colors.black,
                routeTo: '',
            ),
            body: Container(
                margin: EdgeInsets.all(20),
                child: Container(
                    padding: EdgeInsets.only(left: 5),
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                            Expanded(
                                child: Row(
                                    children: [
                                        Image.asset('assets/images/januar.png'),
                                        SizedBox(width: 5),
                                        Image.asset('assets/images/februar.png'),
                                        SizedBox(width: 5),
                                        Image.asset('assets/images/marz.png'),
                                    ]
                                )
                            ),
                            Expanded(
                                child: Row(
                                    children: [
                                        Image.asset('assets/images/april.png'),
                                        SizedBox(width: 5),
                                        Image.asset('assets/images/mai.png'),
                                        SizedBox(width: 5),
                                        Image.asset('assets/images/juni.png'),
                                    ]
                                )
                            ),
                            Expanded(
                                child: Row(
                                    children: [
                                        Image.asset('assets/images/juli.png'),
                                        SizedBox(width: 5),
                                        Image.asset('assets/images/august.png'),
                                        SizedBox(width: 5),
                                        Image.asset('assets/images/september.png'),
                                    ]
                                )
                            ),
                            Expanded(
                                child: Row(
                                    children: [
                                        Image.asset('assets/images/oktober.png'),
                                        SizedBox(width: 5),
                                        Image.asset('assets/images/november.png'),
                                        SizedBox(width: 5),
                                        Image.asset('assets/images/dezember.png'),
                                    ]
                                )
                            )
                        ]
                    )
                )
            )
        );
    }
}