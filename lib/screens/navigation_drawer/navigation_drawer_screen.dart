import 'package:flutter/material.dart';

import '../../widgets/custom_appbar.dart';

import 'components/menu_item.dart';

class NavigationDrawerScreen extends StatelessWidget {
    @override
    Widget build(BuildContext context) {
        return Scaffold(
            body: Container(
                color: Colors.white,
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                        CustomAppBar(
                            leading: 'assets/icons/close.png', 
                            bgColor: Colors.white,
                            fontColor: Colors.black, 
                            trailingWH: ['assets/images/flutter_logo.png', 19.15, 31.91],
                            routeTo: ''
                        ),
                        SizedBox(height: 20),
                        MenuItem(
                            icon: 'assets/icons/user.png', 
                            title: 'Mein Konto', 
                            isActive: false,
                            routeTo: '/my-account'
                        ),
                        MenuItem(
                            icon: 'assets/icons/visiting_card.png', 
                            title: 'Visitenkarte', 
                            isActive: false,
                            routeTo: '/visiting'
                        ),
                        MenuItem(
                            icon: 'assets/icons/time_tracking.png', 
                            title: 'Zeiterfassung', 
                            isActive: true,
                            routeTo: '/time-tracking'
                        ),
                        MenuItem(
                            icon: 'assets/icons/stakes.png', 
                            title: 'Meine Einsätze', 
                            isActive: false,
                        )   
                    ]
                )
            )
        );
    }
}