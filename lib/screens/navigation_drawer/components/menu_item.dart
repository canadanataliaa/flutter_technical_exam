import 'package:flutter/material.dart';

class MenuItem extends StatefulWidget {
    final String icon;
    final String title;
    late bool isActive;
    final String? routeTo;

    MenuItem({
        required this.icon,
        required this.title,
        required this.isActive,
        this.routeTo
    });

  @override
  _MenuItemState createState() => _MenuItemState();
}

class _MenuItemState extends State<MenuItem> {
    @override
    Widget build(BuildContext context) {
        Widget menuIcon = Container(
            margin: EdgeInsets.only(bottom: 15.61),
            child: Image.asset(widget.icon, color: Color.fromRGBO(25, 29, 38, 0.2)),
        );

        Widget menuTitle = Container(
            margin: EdgeInsets.only(bottom: 36),
            child: Column(
                children: [
                    Text(
                        widget.title, 
                        style: TextStyle(
                            fontFamily: 'Roboto', 
                            fontSize: 14,
                            color: widget.isActive == true ? Colors.black : Color.fromRGBO(25, 29, 38, 0.7)
                        )
                    ),
                    if(widget.isActive) 
                    Container(
                        width: 93,
                        height: 4,
                        margin: EdgeInsets.only(top: 8),
                        color: Color.fromRGBO(132, 101, 255, 1),
                    )
                ]
            )
        );

        Widget menuItem = InkWell(
            child: Container(
                width: 130,
                margin: EdgeInsets.only(left: 20),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                        menuIcon,
                        menuTitle
                    ]
                )
            ),
            onTap: () {
                setState(() {
                    widget.isActive = !widget.isActive;
                });
                if (widget.routeTo != null) {
                    Navigator.popAndPushNamed(context, widget.routeTo as String);
                }
            }
            
        );
        
        return menuItem;
    }
}