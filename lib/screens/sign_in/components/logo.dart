import 'package:flutter/material.dart';

class Logo extends StatelessWidget {
    final String image;

    Logo({ required this.image });

    @override
    Widget build(BuildContext context) {
        return Container(
            margin: EdgeInsets.only(top: 163),
            child: Center(
                child: Image.asset(image)
            ),
        );
    }
}