import 'package:flutter/material.dart';

class FieldPassTitle extends StatelessWidget {
    @override
    Widget build(BuildContext context) {
        var textStyle = TextStyle(
            fontFamily: 'Roboto', 
            fontSize: 14,
        );

        return Container(
            width: 363,
            height: 51,
            margin: EdgeInsets.only(top: 48, bottom: 56),
            child: Center(
                child: Text('Flutter Fieldpass', style: textStyle)
            )
        );
    }
}