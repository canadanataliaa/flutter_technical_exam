import 'package:flutter/material.dart';

class SignInBox extends StatelessWidget {
    final String icon;
    final String title;

    SignInBox({
        required this.icon,
        required this.title
    });

    @override
    Widget build(BuildContext context) {
        Widget logo = Container(
            padding: EdgeInsets.only(left: 15, top: 16),
            child: Image.asset(icon)
        );

        Widget txtSignIn = Container(
            padding: EdgeInsets.only(left: 13, top: 20),
            child: Text(
                title,
                style: TextStyle(
                    fontFamily: 'Mulish',
                    fontSize: 14
                )
            )
        );

        Widget btnArrow = Container(
            padding: EdgeInsets.only(left: 68, top: 20),
            child: Image.asset('assets/icons/four_arrow.png'),
        );

        Widget signinBox = Row(
            children: [
                logo,
                txtSignIn,
                btnArrow,
            ]
        );

        Widget borderBottom = Container(
            width: 302,
            height: 3,
            margin: EdgeInsets.only(top: 13),
            color: Color.fromRGBO(224, 224, 224, 1)
        );

        return InkWell(
          child: Container(
              width: 302,
              height: 57,
              child: Column(
                  children: [
                      signinBox,
                      borderBottom
                  ]
              )
          ),
          onTap: () => Navigator.pushNamed(context, '/my-account')
        );
    }
}