import 'package:flutter/material.dart';

class Footer extends StatelessWidget {
    final String title1;
    final String title2;

    Footer({
        required this.title1,
        required this.title2
    });

    @override
    Widget build(BuildContext context) {
        var textStyle = TextStyle(
            fontFamily: 'Mulish',
            fontSize: 14
        );

        return Container(
            margin: EdgeInsets.only(top: 282),
            width: 188,
            height: 16,
            child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                    Text(title1, style: textStyle),
                    SizedBox(width: 32),
                    Text(title2, style: textStyle)
                ]
            )
        );
    }
}