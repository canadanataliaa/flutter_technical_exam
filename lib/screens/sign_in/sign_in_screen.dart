import 'package:flutter/material.dart';

import 'components/fieldpass_title.dart';
import 'components/footer.dart';
import 'components/logo.dart';
import 'components/signin_box.dart';

class SignInScreen extends StatelessWidget {
    @override
    Widget build(BuildContext context) {
        return Scaffold(
            body: SingleChildScrollView(
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                      Logo(image: 'assets/images/flutter_logo.png'),
                      FieldPassTitle(),
                      SignInBox(icon: 'assets/icons/microsoft_logo.png', title: 'Sign in with Microsoft'),
                      Footer(title1: 'Impressum', title2: 'Datenschutz')
                  ]
              ),
            )
        );
    }
}