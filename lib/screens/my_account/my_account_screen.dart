import 'package:flutter/material.dart';

import '../../widgets/custom_appbar.dart';

import 'components/myaccount_view.dart';

class MyAccountScreen extends StatelessWidget {
	@override
	Widget build(BuildContext context) {
		return Scaffold(
			backgroundColor: Color.fromRGBO(245, 245, 245, 1),
            appBar: CustomAppBar(
                leading: 'assets/icons/menu.png',
                bgColor: Colors.white,
                fontColor: Colors.black,
                routeTo: '/side-menu'
            ),
			body: Container(
                child: MyAccountView()
            )
		);
	}
}