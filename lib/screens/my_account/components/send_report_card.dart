import 'package:flutter/material.dart';

import '../../../widgets/custom_button.dart';

class SendReportCard extends StatelessWidget {
	final String title;
	final String date;
	final String btnTitle;

	SendReportCard({
		required this.title,
		required this.date,
		required this.btnTitle
	});

	@override
	Widget build(BuildContext context) {
		Widget iconCalendar = Container(
			child: Image.asset('assets/icons/kalender.png'),
		);

		Widget reportDetails = Container(
			padding: EdgeInsets.only(left: 25.88),
			child: Column(
				crossAxisAlignment: CrossAxisAlignment.start,
				children: [
					Text(
                        date, 
                        style: TextStyle(
                            fontFamily: 'Allerta-Stencil', 
                            fontSize: 16,
                            letterSpacing: -1.2
                        )
                    ),
					SizedBox(height: 16),
					CustomButton(
                        title: btnTitle,
                        routeTo: '',
                        width: 220,
                        height: 42,
                    )
				]
			)
		);

		return Card(
            margin: EdgeInsets.only(left: 16, bottom: 32),
            elevation: 0,
			child: Column(
				crossAxisAlignment: CrossAxisAlignment.start,
				children: [
					Text(
                        title, 
                        style: TextStyle(
                            fontFamily: 'Allerta-Stencil', 
                            fontSize: 22, 
                            letterSpacing: -1.3
                        )
                    ),
					SizedBox(height: 18),
					Row(
						crossAxisAlignment: CrossAxisAlignment.start,
						children: [
							iconCalendar,
							reportDetails
						]
					)
				]
			)
		);
	}
}