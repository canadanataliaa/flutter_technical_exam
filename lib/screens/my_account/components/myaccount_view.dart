import 'package:flutter/material.dart';

import '../../../widgets/myaccount_card.dart';

import 'contact_person.dart';
import 'send_report_card.dart';

class MyAccountView extends StatelessWidget {
    @override
    Widget build(BuildContext context) {
        return Container(
            color: Colors.white,
            margin: EdgeInsets.only(top: 16, left: 16, right: 16),
            child: SingleChildScrollView(
                child: Column(
                    children: [
                        MyAccountCard(
                            title: 'Mein Konto',
                            image: 'assets/images/profile_avatar.png',
                            name: 'Greg Neu',
                            email: 'max.mustermann@bkl.de',
                            fullUnderline: false,
                            assembler: 'Monteur',
                        ),
                        ContactPersonCard(
                            image: 'assets/images/contact_avatar.png', 
                            name: 'Ingo Flamingo', 
                            email: 'ingo.flamingo@f-bootcamp.com', 
                            number: '0160 - 123456789'
                        ),
                        SendReportCard(
                            title: 'Wochenbericht',
                            date: '12.03 - 19.03.2021',
                            btnTitle: 'Wochenbericht zuschicken',
                        ),
                        SendReportCard(
                            title: 'Monatsbericht',
                            date: 'April 2020',
                            btnTitle: 'Monatsbericht erstellen',
                        )
                    ]
                )
            )
        );
    }
}