import 'package:flutter/material.dart';

import '../../../widgets/custom_text_bg.dart';

class ContactPersonCard extends StatelessWidget {
	final String image;
	final String name;
	final String email;
	final String number;

	const ContactPersonCard({
		required this.image,
		required this.name,
		required this.email,
		required this.number
	});

	@override
	Widget build(BuildContext context) {
		var h1Style = TextStyle(fontFamily: 'Allerta-Stencil', fontSize: 22, letterSpacing: -1.3);
		var h2Style = TextStyle(fontFamily: 'Allerta-Stencil', fontSize: 16, letterSpacing: -1.3);
		var subStyle = TextStyle(fontFamily: 'Mulish', fontSize: 12);

		Widget avatar = Container(
			child: Image.asset(image),
		);

		Widget contactDetails = Container(
            margin: EdgeInsets.only(left: 16),
			child: Column(
				crossAxisAlignment: CrossAxisAlignment.start,
				children: [
					Text(name, style: h2Style),
                    SizedBox(height: 2),
					Text(email, style: subStyle),
					SizedBox(height: 8),
                    CustomTextBG(width: 130, text: number)
				],
			),
		);

		return Card(
            margin: EdgeInsets.only(left: 16, bottom: 32),
            elevation: 0,
			child: Column(
				crossAxisAlignment: CrossAxisAlignment.start,
				children: [
					Text('Ansprechpartner', style: h1Style),
					SizedBox(height: 16),
					Row(
						children: [
							avatar,
							contactDetails
						]
					)
				]
			)
		);
	}
}