// @dart=2.9
import 'package:flutter/material.dart';

import 'package:timetable/timetable.dart';
import 'package:time_machine/time_machine.dart';

import 'components/time_tracking_appbar.dart';

class TimeTrackingScreen extends StatelessWidget {
    @override
    Widget build(BuildContext context) {
        final myEventProvider = EventProvider.list([
            BasicEvent(
                id: 0,
                title: 'Projekt 1233',
                color: Colors.blue,
                start: LocalDate.today().at(LocalTime(13, 0, 0)),
                end: LocalDate.today().at(LocalTime(15, 0, 0)),
            ),
        ]);

        final myController = TimetableController(
            eventProvider: myEventProvider,
            // Optional parameters with their default values:
            initialTimeRange: InitialTimeRange.range(
                startTime: LocalTime(8, 0, 0),
                endTime: LocalTime(20, 0, 0),
            ),
            initialDate: LocalDate.today(),
            visibleRange: VisibleRange.week(),
            firstDayOfWeek: DayOfWeek.monday,
        );

        return Scaffold(
            appBar: TimeTrackingAppBar(
                leading: 'assets/icons/menu.png',
                title: 'Donnerstag',
                subtitles: ['Offen', '12.01.2021'],
                trailings: ['assets/icons/calendar_button.png', 'assets/icons/add_black.png'],
                bgColor: Colors.white,
                leadingRoute: '/side-menu',
                trailingRoutes: ['', ''],
            ),
            body: Container(
                child: Timetable<BasicEvent>(
                    controller: myController,
                    eventBuilder: (event) => BasicEventWidget(event),
                    allDayEventBuilder: (context, event, info) =>
                      BasicAllDayEventWidget(event, info: info),
                )
            )
        );
    }
}