import 'package:flutter/material.dart';

import '../../../widgets/custom_text_bg.dart';

import 'day_icon.dart';

class TimeTrackingAppBar extends StatelessWidget implements PreferredSizeWidget {
    final String leading;
    final String title;
    final List<String> subtitles;
    final List<String> trailings;
    final Color bgColor;
    final String leadingRoute;
    final List<String> trailingRoutes;

    TimeTrackingAppBar({
        required this.leading,
        required this.title,
        required this.subtitles,
        required this.trailings,
        required this.bgColor,
        required this.leadingRoute,
        required this.trailingRoutes
    });

    @override
    Size get preferredSize => const Size.fromHeight(140);

    @override
    Widget build(BuildContext context) {
        Widget menuBtn = IconButton(
            icon: Image.asset(leading), 
            onPressed: () => Navigator.pushNamed(context, leadingRoute)
        );

        Widget appBarTitle = Container(
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                    Text(title, style: TextStyle(fontFamily: 'Allerta-Stencil', fontSize: 22, color: Colors.black)),
                    Row(
                        children: [
                            CustomTextBG(width: 60, text: subtitles[0]),
                            Text(subtitles[1], style: TextStyle(fontFamily: 'Mulish', fontSize: 12, color: Color.fromRGBO(25, 29, 38, 0.7)))
                        ]
                    )
                ]
            ),
        );

        Widget calendarBtn = InkWell(
            onTap: () => Navigator.pushNamed(context, '/calendar'), 
            child: Image.asset(trailings[0])
        );

        Widget addBtn = InkWell(
            onTap: () => Navigator.pushNamed(context, '/add-time-tracking'), 
            child: Container(
                width: 42,
                height: 42,
                margin: EdgeInsets.only(right: 28),
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: Color.fromRGBO(224, 224, 224, 1)
                ),
                child: Image.asset(trailings[1]),
            )
        );

        return AppBar(
            elevation: 0,
            leadingWidth: 75,
            leading: menuBtn,
            titleSpacing: 1,
            title: appBarTitle,
            backgroundColor: Colors.white,
            actions: [calendarBtn, addBtn],
            flexibleSpace: Stack(
                children: [
                    Positioned(
                        bottom: 5,
                        left: 18,
                        right: 18,
                        child: Container(
                            height: 60,
                            child: Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                    DayIcon(
                                        color: Color.fromRGBO(225, 65, 65, 1),
                                        icon: 'assets/icons/check_small.png',
                                        day: 'Mo',
                                    ),
                                    DayIcon(
                                        color: Color.fromRGBO(225, 183, 43, 1),
                                        icon: 'assets/icons/check_small.png',
                                        day: 'Di',
                                    ),
                                    DayIcon(
                                        color: Color.fromRGBO(103, 136, 255, 1),
                                        icon: 'assets/icons/send_small.png',
                                        day: 'Mi',
                                    ),
                                    DayIcon(
                                        color: Color.fromRGBO(132, 101, 255, 1),
                                        icon: 'assets/icons/edit_small.png',
                                        day: 'Do',
                                    ),
                                    DayIcon(
                                        color: Colors.grey.shade100,
                                        icon: '',
                                        day: 'Fr',
                                    ),
                                    DayIcon(
                                        color: Colors.grey.shade100,
                                        icon: '',
                                        day: 'Sa',
                                    ),
                                    DayIcon(
                                        color: Colors.grey.shade100,
                                        icon: '',
                                        day: 'So',
                                    )
                                ]
                            )
                        )
                    ) 
                ]
            )
        );
    }
}