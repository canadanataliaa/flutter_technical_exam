import 'package:flutter/material.dart';

class DayIcon extends StatelessWidget {
    final Color color;
    final String icon;
    final String day;

    DayIcon({
        required this.color,
        required this.icon,
        required this.day
    });

    @override
    Widget build(BuildContext context) {
        return Container(
            margin: EdgeInsets.only(right: 8),
            child: Column(
                children: [
                    Container(
                        width: 42,
                        height: 42,
                        margin: EdgeInsets.only(bottom: 2),
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: color
                        ),
                        child: icon != '' 
                            ? Image.asset(icon) 
                            : Center(child: Text(day, style: TextStyle(fontFamily: 'Allerta-Stencil', fontSize: 21, color: Colors.black12)))
                    ),
                    Text(day, style: TextStyle(fontFamily: 'Allerta-Stencil', fontSize: 12))
                ]
            ),
        );
    }
}