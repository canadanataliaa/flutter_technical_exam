import 'package:flutter/material.dart';

import '../../screens/add_time_tracking/components/pause_tab.dart';
import '../../widgets/custom_appbar.dart';
import '../../widgets/custom_tabbar.dart';

import 'components/working_time_tab.dart';

class AddTimeTrackingScreen extends StatefulWidget {
    @override
    _AddTimeTrackingScreenState createState() => _AddTimeTrackingScreenState();
}

class _AddTimeTrackingScreenState extends State<AddTimeTrackingScreen> with SingleTickerProviderStateMixin {
    late TabController _controller;

    @override
    void initState() {
        super.initState();
        _controller = TabController(length: 2, vsync: this);
    }

    @override
    Widget build(BuildContext context) {
        return Scaffold(
            backgroundColor: Colors.white,
            appBar: CustomAppBar(
                leading: 'assets/icons/close.png', 
                bgColor: Colors.white,
                fontColor: Colors.black, 
                trailingWH: ['assets/images/flutter_logo.png', 23.94, 39.89],
                routeTo: '/time-tracking'
            ),
            body: Column(
                children: [
                    CustomTabBar(
                        tabTitles: ['Arbeitszeit', 'Pause'], 
                        controller: _controller,
                    ),
                    Expanded(
                        child: TabBarView(
                            controller: _controller,
                            children: [
                                // Arbeitszeit TabView
                                WorkingTimeTab(),
                                // Pause TabView
                                PauseTab()
                            ]
                        )
                    )
                ]
            )
        );
    }
}