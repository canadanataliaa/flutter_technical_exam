import 'package:flutter/material.dart';

import '../../../widgets/comment_card.dart';
import '../../../widgets/custom_appbar.dart';
import '../../../widgets/time_picker_card.dart';

class PauseScreen extends StatefulWidget {
    final String? category;
    final String? projectId;
    // time

    PauseScreen([
        this.category,
        this.projectId
    ]);

  @override
  _PauseScreenState createState() => _PauseScreenState();
}

class _PauseScreenState extends State<PauseScreen> {
    @override
    Widget build(BuildContext context) {
        List<DropdownMenuItem> categories = ['Baustellenvorbereitung']
            .map((category) {
                return DropdownMenuItem(
                    value: category,
                    child: Text(category)
                );
            }).toList();
        
        DropdownButtonFormField dropdownCategory = DropdownButtonFormField(
            icon: Image.asset('assets/icons/arrow_down.png'),
            isExpanded: true,
            itemHeight: 68,
            value: widget.category,
            items: categories,
            onChanged: (value) {
                setState(() {
                });
            }
        );

        return Scaffold(
            backgroundColor: Colors.white,
            appBar: CustomAppBar(
                leading: 'assets/icons/arrow_back.png', 
                bgColor: Color.fromRGBO(103, 136, 255, 1),
                title: 'Pause',
                subtitle: 'Projekt: ' + widget.projectId.toString(),
                fontColor: Colors.white, 
                routeTo: '/add-time-tracking'
            ),
            body: SingleChildScrollView(
                child: Container(
                    padding: EdgeInsets.symmetric(horizontal: 16, vertical: 25),
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                            dropdownCategory,
                            SizedBox(height: 20),
                            TimePickerCard(
                                title: 'Pause',
                                borderColor: Color.fromRGBO(103, 136, 255, 1),
                            ),
                            CommentCard()
                        ]
                    )
                ),
            )
        );
    }
}

        