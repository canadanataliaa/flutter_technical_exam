import 'package:flutter/material.dart';

import '../../../widgets/add_edit_card.dart';
import '../../../widgets/comment_card.dart';
import '../../../widgets/time_picker_card.dart';

class WorkingTimeTab extends StatefulWidget {
    final String? category;
    final String? projectId;

    WorkingTimeTab([
        this.category,
        this.projectId
    ]);

    @override
    _WorkingTimeTabState createState() => _WorkingTimeTabState();
}

class _WorkingTimeTabState extends State<WorkingTimeTab> {
    List<DropdownMenuItem> categories = ['Baustellenvorbereitung']
        .map((category) {
            return DropdownMenuItem(
                value: category,
                child: Text(category)
            );
        }).toList();

    List<DropdownMenuItem> projectIds = ['1298721398']
        .map((projectId) {
            return DropdownMenuItem(
                value: projectId,
                child: Text(projectId)
            );
        }).toList();

    String selectedCategory = '';
    String selectedProjectId = '';

    bool isCategorySelected = false;
    bool isProjectIdSelected = false;

    @override
    Widget build(BuildContext context) {
        Widget generateTitle(String title) {
            return Text(
                title, 
                style: TextStyle(
                    fontFamily: 'Allerta-Stencil', 
                    fontSize: 22,
                    letterSpacing: -1.3,
                )
            );
        }

        DropdownButtonFormField dropdownCategory = DropdownButtonFormField(
            icon: Image.asset('assets/icons/arrow_down.png'),
            isExpanded: true,
            itemHeight: 80,
            // value: widget.category != null ? widget.category : null,
            decoration: InputDecoration(
                hintText: 'Wahlen Sie bitte Kategorie aus',
                hintStyle: TextStyle(
                    fontFamily: 'Mulish', 
                    fontSize: 14, 
                    color: Color.fromRGBO(185, 185, 185, 1)
                )
            ),
            items: categories,
            onChanged: (value) {
                setState(() {
                    selectedCategory = value;
                    isCategorySelected = true;
                });
            }
        );

        DropdownButtonFormField dropdownProjectId = DropdownButtonFormField(
            icon: Image.asset('assets/icons/arrow_down.png'),
            isExpanded: true,
            itemHeight: 68,
            // value: widget.projectId != null ? widget.projectId : null,
            decoration: InputDecoration(
                hintText: 'Projektnummer hinzufügen',
                hintStyle: TextStyle(
                    fontFamily: 'Mulish', 
                    fontSize: 14, 
                    color: Color.fromRGBO(185, 185, 185, 1)
                )
            ),
            items: projectIds,
            onChanged: (value) {
                setState(() {
                    selectedProjectId = value;
                    isProjectIdSelected = true;
                });
            }
        );

        Widget dropdownFormFields = Container(
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                    generateTitle('Kategorie'),
                    dropdownCategory,
                    SizedBox(height: 20),
                    generateTitle('Projektnummer'),
                    dropdownProjectId,
                    SizedBox(height: 30),
                ]
            )
        );

        List<Widget> showContent(bool isBothSelected) {
            if (!isBothSelected) {
                return [
                    AddorEditCard(
                        title: 'Mitarbeiter',
                        btnColor: Colors.black,
                        category: selectedCategory,
                        projectId: selectedProjectId
                    ),
                    TimePickerCard(
                        title: 'Arbeitszeit',
                        borderColor: Color.fromRGBO(76, 183, 229, 1),
                    ),
                    CommentCard()
                ];
            }

            return [
                AddorEditCard(
                    title: 'Mitarbeiter', 
                    btnColor: Colors.black,
                    tag: 'Greg Neu',
                    tagWidth: 130,
                    routeTo: '/pause',
                    category: selectedCategory,
                    projectId: selectedProjectId
                ),
                TimePickerCard(
                    title: 'Arbeitszeit',
                    borderColor: Color.fromRGBO(76, 183, 229, 1),
                ),
                AddorEditCard(
                    title: 'Pause', 
                    btnColor: Color.fromRGBO(103, 136, 225, 1),
                    routeTo: '/pause',
                    category: selectedCategory,
                    projectId: selectedProjectId
                ),
                // WaitingPeriod Add
                AddorEditCard(
                    title: 'Wartezeit', 
                    btnColor: Color.fromRGBO(255, 183, 43, 1),
                    // routeTo: '/waiting-period',
                    category: selectedCategory,
                    projectId: selectedProjectId
                ),
                // StandbyTime Add
                AddorEditCard(
                    title: 'Bereitschaftszeit', 
                    btnColor: Color.fromRGBO(132, 101, 255, 1),
                    tag: '08:00 Uhr - 08 : 30 Uhr',
                    tagWidth: 197,
                    routeTo: '/standby-time',
                    category: selectedCategory,
                    projectId: selectedProjectId
                ),
                CommentCard(),
            ];   
        }

        return SingleChildScrollView(
            child: Container(
                padding: EdgeInsets.symmetric(horizontal: 16, vertical: 25),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                        dropdownFormFields,
                        ...showContent(isCategorySelected && isProjectIdSelected)
                        // Get employee.name with projektnumm and kategori selected from dropdown
                        // showContent(),

                        // AddorEditCard with employee.name and edit button
                        // TimePickerCard
                        // Pause Add
                    ]
                )
            )
        );
    }
}