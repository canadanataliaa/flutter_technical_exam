import 'package:flutter/material.dart';

import '../../../widgets/comment_card.dart';
import '../../../widgets/custom_appbar.dart';
import '../../../widgets/time_picker_card.dart';

class StandByTimeScreen extends StatefulWidget {
    final String? category;
    final String? projectId;
    // time

    StandByTimeScreen([
        this.category,
        this.projectId
    ]);

  @override
  _StandByTimeScreenState createState() => _StandByTimeScreenState();
}

class _StandByTimeScreenState extends State<StandByTimeScreen> {
    List<DropdownMenuItem> categories = ['Baustellenvorbereitung']
        .map((category) {
            return DropdownMenuItem(
                value: category,
                child: Text(category)
            );
        }).toList();
    
    @override
    Widget build(BuildContext context) {
        Widget generateTitle(String title) {
            return Text(
                title, 
                style: TextStyle(
                    fontFamily: 'Allerta-Stencil', 
                    fontSize: 22,
                    letterSpacing: -1.3,
                )
            );
        }

        DropdownButtonFormField dropdownCategory = DropdownButtonFormField(
            icon: Image.asset('assets/icons/arrow_down.png'),
            isExpanded: true,
            itemHeight: 68,
            value: widget.category,
            items: categories,
            onChanged: (value) {
                setState(() {
                });
            }
        );

        return Scaffold(
            backgroundColor: Colors.white,
            appBar: CustomAppBar(
                leading: 'assets/icons/arrow_back.png', 
                bgColor: Color.fromRGBO(132, 101, 255, 1),
                title: 'Bereitschaftszeit',
                subtitle: 'Projekt: ' + widget.projectId.toString(),    // $projectnum from the selected value in the dropdownmenu 
                fontColor: Colors.white, 
                routeTo: '/add-time-tracking'
            ),
            body: SingleChildScrollView(
                child: Container(
                    padding: EdgeInsets.symmetric(horizontal: 16, vertical: 25),
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                            generateTitle('Kategorie'),
                            dropdownCategory,
                            SizedBox(height: 20),
                            TimePickerCard(
                                title: 'Bereitschaftszeit',
                                borderColor: Color.fromRGBO(132, 101, 255, 1),
                            ),
                            CommentCard()
                        ]
                    )
                ),
            )
        );
    }
}