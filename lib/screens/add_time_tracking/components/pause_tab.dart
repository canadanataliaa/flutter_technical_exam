import 'package:flutter/material.dart';

import '../../../widgets/comment_card.dart';
import '../../../widgets/time_picker_card.dart';

class PauseTab extends StatefulWidget {
  @override
  _PauseTabState createState() => _PauseTabState();
}

class _PauseTabState extends State<PauseTab> {
    List<DropdownMenuItem> categories = ['Baustellenvorbereitung']
        .map((category) {
            return DropdownMenuItem(
                value: category,
                child: Text(category)
            );
        }).toList();
    
    String selectedCategory = '';

    @override
    Widget build(BuildContext context) {
        Widget generateTitle(String title) {
            return Text(
                title, 
                style: TextStyle(
                    fontFamily: 'Allerta-Stencil', 
                    fontSize: 22,
                    letterSpacing: -1.3,
                )
            );
        }

        DropdownButtonFormField dropdownCategory = DropdownButtonFormField(
            icon: Image.asset('assets/icons/arrow_down.png'),
            isExpanded: true,
            itemHeight: 68,
            decoration: InputDecoration(
                hintText: 'Wahlen Sie bitte Kategorie aus',
                hintStyle: TextStyle(
                    fontFamily: 'Mulish', 
                    fontSize: 14, 
                    color: Color.fromRGBO(185, 185, 185, 1)
                )
            ),
            items: categories,
            onChanged: (value) {
                setState(() {
                    selectedCategory = value;
                });
            }
        );

        return SingleChildScrollView(
            child: Container(
                padding: EdgeInsets.symmetric(horizontal: 16, vertical: 25),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                        generateTitle('Kategorie'),
                        dropdownCategory,
                        SizedBox(height: 20),
                        TimePickerCard(
                            title: 'Pause',
                            borderColor: Color.fromRGBO(103, 136, 255, 1),
                        ),
                        CommentCard()
                    ]
                )
            )
        );
    }
}