# Time Tracking App

# Packages

- Flutter SDK: >=2.12.0 <3.0.0
- [flutter_time_picker_spinner: ^2.0.0](https://pub.dev/packages/flutter_time_picker_spinner)
- [timetable: ^0.2.9](https://pub.dev/packages/timetable)

# Flutter Version

- [Flutter 2.2.3](https://flutter.dev/docs/development/tools/sdk/releases)

# Project UI Features

### Sign In

- Sign in with Microsoft

### My Account

- View personal information
- View contact person
- Generate Weekly/Monthly Report

### Visiting Card

- View my business card
- View Supervisor business card

### Time Tracking

- View calendar year
- View timetable
- Add time slots (with and without working time)
- Apply Vacation/Special/Sick leaves
- Edit/cancel leaves
- View pause time
- View standby time
- Save working time
- View approved working times

# Screenshots

![Untitled](readme_images/Untitled.png)

![Untitled](readme_images/Untitled%201.png)

![Untitled](readme_images/Untitled%202.png)

![Untitled](readme_images/Untitled%203.png)

![Untitled](readme_images/Untitled%204.png)

![Untitled](readme_images/Untitled%205.png)

![Untitled](readme_images/Untitled%206.png)

![Untitled](readme_images/Untitled%207.png)

![Untitled](readme_images/Untitled%208.png)

Static calendar image

![Untitled](readme_images/Untitled%209.png)

![Untitled](readme_images/Untitled%2010.png)

![Untitled](readme_images/Untitled%2011.png)

![Untitled](readme_images/Untitled%2012.png)

![Untitled](readme_images/Untitled%2013.png)

![Untitled](readme_images/Untitled%2014.png)

![Untitled](readme_images/Untitled%2015.png)

![Untitled](readme_images/Untitled%2016.png)